const webpack = require('webpack')
const path = require('path')
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin')
const meta = require('./package.json')
const CompressionWebpackPlugin = require("compression-webpack-plugin");
const TerserPlugin = require('terser-webpack-plugin');

const config = {
  mode: 'development',
  devtool: "inline-source-map",
  entry: './src/main.ts',
  module: {
    rules: [
      {
        test: /.tsx?$/,
        use: [
          {
            loader: 'ts-loader',
            options: {
              configFile: __dirname + '/tsconfig.json',
              allowTsInNodeModules: true,
            }
          }
        ],
        // exclude: /node_modules/,
      },
      {
        test: /\.sass$/, use: [
          { loader: 'style-loader' },
          { loader: 'css-modules-typescript-loader' },
          { loader: 'css-loader', options: { modules: true } },
          { loader: 'sass-loader' }
        ]
      },
      { test: /\.(png|jpg|jpeg|gif|svg)$/, use: 'url-loader?limit=25000' },
      {
        test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/, use: [{
          loader: 'file-loader',
          options: {
            name: '[name].[ext]',
            outputPath: 'fonts/'
          }
        }]
      },
    ],
  },
  plugins: [
    new HtmlWebpackPlugin({
      title: `${meta.description}`,
      template: './resources/index.html'
    }),
    new webpack.HashedModuleIdsPlugin(),
  ].concat(

  ),
  resolve: {
    extensions: ['.tsx', '.ts', '.js'],
  },
  output: {
    path: path.join(__dirname, 'public'),
    filename: '[name].[contenthash].js',
  },
  optimization: {
    splitChunks: {
      cacheGroups: {
        vendor: {
          test: /[\\/]node_modules[\\/]/,
          name: 'vendors',
          chunks: 'all'
        }
      }
    }
  },
  devServer: {
    contentBase: path.join(__dirname, 'public'),
    compress: true,
    port: 3000,
    host: '0.0.0.0',
    allowedHosts: [
      'era.islanddragon.org'
    ]
  }
};

module.exports = (env, argv) => {
  if (argv.mode === 'production') {
    config.plugins.push(new CompressionWebpackPlugin())

    config.plugins.push(new CleanWebpackPlugin({
      cleanAfterEveryBuildPatterns: ['public'],
    }))

    config.optimization = {
      minimize: true,
      minimizer: [new TerserPlugin()],
    }
  }

  return config
}
