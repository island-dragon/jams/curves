# Requirements

- Git
- Node 13.8.0
- Yarn

# Instructions

- yarn initialize
- yarn start
- visit localhost:3000

## Working with tingine

- cd tingine
- git checkout master
- Work as normal
- cd ..

Make sure to commit main repo when after doing any changes to tingine, so that the pointer is updated
